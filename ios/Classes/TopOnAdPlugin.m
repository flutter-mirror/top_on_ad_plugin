#import "TopOnAdPlugin.h"
#if __has_include(<top_on_ad_plugin/top_on_ad_plugin-Swift.h>)
#import <top_on_ad_plugin/top_on_ad_plugin-Swift.h>
#else
// Support project import fallback if the generated compatibility header
// is not copied when this plugin is created as a library.
// https://forums.swift.org/t/swift-static-libraries-dont-copy-generated-objective-c-header/19816
#import "top_on_ad_plugin-Swift.h"
#endif

@implementation TopOnAdPlugin
+ (void)registerWithRegistrar:(NSObject<FlutterPluginRegistrar>*)registrar {
  [SwiftTopOnAdPlugin registerWithRegistrar:registrar];
}
@end
