import 'package:ad_common/third_ad.dart';
import 'package:ad_common/third_ad_instance.dart';
import 'package:flutter/material.dart';
import 'package:top_on_ad_plugin_example/plugin_init.dart';
void main() {
  runApp(const MyApp());
}

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  ThirdAd? thirdAd;

  @override
  void initState() {
    super.initState();
    initAdPlugin();
  }

  // Platform messages are asynchronous, so we initialize in an async method.

  void initAdSdk(){
    ThirdAdParams thirdAdParams = ThirdAdParams(appId: 'a632d14ba5976f',
        appKey: '3f8fcd7027f2f2d25796e8f42698ce33',
        placementId: 'b632d14d604f0b',
        thirdAdEventCallback: (AdCallbackEvent adCallbackEvent,String message,Object? extraInfo){
          debugPrint("ad callback $adCallbackEvent message:$message");
        });
    if(thirdAdInstance.adSourceMap.containsKey(ThirdAdType.topOnRewardVideoAd)) {
      thirdAd = thirdAdInstance.adSourceMap[ThirdAdType.topOnRewardVideoAd](thirdAdParams);
    }
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: const Text('Plugin example app'),
        ),
        body: Center(
          child: Column(
            children: [
              ElevatedButton(onPressed: () {
                initAdSdk();
                thirdAd?.load();
              }, child: const Text("load video ad"),),
              ElevatedButton(onPressed: () {
                thirdAd?.show();
              }, child: const Text("play video ad"),)
            ],
          ),
        ),
      ),
    );
  }
}
