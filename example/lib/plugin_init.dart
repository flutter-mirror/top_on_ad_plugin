import 'package:top_on_ad_plugin/topOn_ad_manager.dart';

/// FileName plugin_init
/// @Author zhuqingfang
/// @Date 2022/9/28 3:49 下午
/// @Description plugin init
void initAdPlugin() {
  topOnAdInstanceInit();
}