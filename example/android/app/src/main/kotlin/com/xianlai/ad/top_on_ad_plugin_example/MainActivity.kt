package com.xianlai.ad.top_on_ad_plugin_example

import io.flutter.embedding.android.FlutterActivity
import io.flutter.embedding.engine.FlutterEngine
import io.flutter.plugin.common.BinaryMessenger

class MainActivity: FlutterActivity() {
    override fun configureFlutterEngine(flutterEngine: FlutterEngine) {
        super.configureFlutterEngine(flutterEngine)
        mBinaryMessenger = flutterEngine.dartExecutor.binaryMessenger
    }

    companion object {
        lateinit var mBinaryMessenger: BinaryMessenger
    }
}
