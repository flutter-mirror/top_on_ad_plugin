package com.xianlai.ad.top_on_ad_plugin_example;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;

import androidx.annotation.NonNull;


import io.flutter.Log;
import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;

/**
 * Created by yufengyang on 2022/3/8 3:18 下午
 *
 * @des methodchannel manager
 */
public class ChannelManager {

    public final MethodChannel methodChannel;
    @SuppressLint("StaticFieldLeak")
    private static volatile ChannelManager sInstance;
    public Context mContext;
    public Activity mActivity;
    private static int mStatusBarHeight;

    public ChannelManager() {
        methodChannel = new MethodChannel(MainActivity.mBinaryMessenger, "top_on_ad_plugin");
        methodChannel.setMethodCallHandler(methodCallHandler);
    }

    public static ChannelManager getsInstance() {
        if (null == sInstance) {
            synchronized (ChannelManager.class) {
                if (null == sInstance) {
                    sInstance = new ChannelManager();
                }
            }
        }
        return sInstance;
    }


    @NonNull
    public final MethodChannel.MethodCallHandler methodCallHandler = new MethodChannel.MethodCallHandler() {
        @Override
        public void onMethodCall(@NonNull MethodCall call, @NonNull MethodChannel.Result result) {

        }
    };

    /**
     * sendMessage
     */
    public void sendMessageToFlutter(String method, Object arguments, MethodChannel.Result callback) {
        methodChannel.invokeMethod(method, arguments, callback);
    }

    /**
     * sendMessage
     */
    public void sendMessageToFlutter(String method, Object arguments) {
        if (methodChannel != null) {
            methodChannel.invokeMethod(method, arguments);
        }
    }

}
