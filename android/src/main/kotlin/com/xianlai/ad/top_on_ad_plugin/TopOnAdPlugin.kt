package com.xianlai.ad.top_on_ad_plugin

import android.util.Log
import androidx.annotation.NonNull

import io.flutter.embedding.engine.plugins.FlutterPlugin
import io.flutter.plugin.common.MethodCall
import io.flutter.plugin.common.MethodChannel
import io.flutter.plugin.common.MethodChannel.MethodCallHandler
import io.flutter.plugin.common.MethodChannel.Result
//import org.greenrobot.eventbus.EventBus
//import org.greenrobot.eventbus.Subscribe
//import org.greenrobot.eventbus.ThreadMode

/** TopOnAdPlugin */
class TopOnAdPlugin: FlutterPlugin, MethodCallHandler {
  /// The MethodChannel that will the communication between Flutter and native Android
  ///
  /// This local reference serves to register the plugin with the Flutter Engine and unregister it
  /// when the Flutter Engine is detached from the Activity
  private lateinit var channel : MethodChannel

  override fun onAttachedToEngine(@NonNull flutterPluginBinding: FlutterPlugin.FlutterPluginBinding) {
    channel = MethodChannel(flutterPluginBinding.binaryMessenger, "top_on_ad_plugin")
    channel.setMethodCallHandler(this)
    // DO OTHER INITIALIZATION BELOW
//    if (!EventBus.getDefault().isRegistered(this)) {
//      EventBus.getDefault().register(this)
//    }
//    EventBus.getDefault().post("TopOnAdExist")
  }

  override fun onMethodCall(@NonNull call: MethodCall, @NonNull result: Result) {
    if (call.method == "initAdSdk") {
//      EventBus.getDefault().post("AdInstanceInit")
      result.success(true)
    } else {
      result.notImplemented()
    }
  }

  override fun onDetachedFromEngine(@NonNull binding: FlutterPlugin.FlutterPluginBinding) {
    channel.setMethodCallHandler(null)
//    if (EventBus.getDefault().isRegistered(this)) {
//      EventBus.getDefault().unregister(this)
//    }
  }

//  @Subscribe(threadMode = ThreadMode.MAIN)
//  fun onAdEvent(event: String) {
//    if ("AdInstanceInit" == event) {
//      Log.d("MainActivity","enter into event bus $event")
//      channel.invokeMethod(event,null)
//    }
//  }
}
