import 'package:ad_common/third_ad.dart';
import 'package:ad_common/third_ad_instance.dart';
import 'package:anythink_sdk/at_init.dart';
import 'package:flutter/foundation.dart';
import 'package:top_on_ad_plugin/top_on_banner_ad.dart';
import 'package:top_on_ad_plugin/top_on_insert_ad.dart';
import 'package:top_on_ad_plugin/top_on_native_ad.dart';
import 'package:top_on_ad_plugin/top_on_reward_video_ad.dart';

/// FileName top_on_ad_manager
/// @Author zhuqingfang
/// @Date 2022/9/26 2:41 下午
/// @Description top on ad manager
//为空初始化，不空，不初始化，确保sdk只初始化一次
String? _sdkInit;

void init({required ThirdAdParams thirdAdParams}) async {
  if (kDebugMode || (thirdAdParams.openLog != null && thirdAdParams.openLog!)) {
    _setLogEnabled();
  }
  //可多次被调用，但只初始化一次
  _sdkInit = _sdkInit ??
      await ATInitManger.initAnyThinkSDK(
          appidStr: thirdAdParams.appId, appidkeyStr: thirdAdParams.appKey);
}

void _setLogEnabled() {
  ATInitManger.setLogEnabled(
    logEnabled: true,
  );
}

void topOnAdInstanceInit() {
  thirdAdInstance.adSourceMap[ThirdAdType.topOnRewardVideoAd] =
      _createTopOnRewardVideoAdInstance;
  thirdAdInstance.adSourceMap[ThirdAdType.topOnInsertAd] =
      _createTopOnInsertAdInstance;
  thirdAdInstance.adSourceMap[ThirdAdType.topOnBannerAd] =
      _createTopOnBannerAdInstance;
  thirdAdInstance.adSourceMap[ThirdAdType.topOnNativeAd] =
      _createTopOnNativeAdInstance;
}

ThirdAd _createTopOnRewardVideoAdInstance(ThirdAdParams thirdAdParams) {
  return TopOnRewardVideoAd(thirdAdParams: thirdAdParams);
}

ThirdAd _createTopOnInsertAdInstance(ThirdAdParams thirdAdParams) {
  return TopOnInsertAd(thirdAdParams: thirdAdParams);
}

ThirdAd _createTopOnBannerAdInstance(ThirdAdParams thirdAdParams) {
  return TopOnBannerAd(thirdAdParams: thirdAdParams);
}

ThirdAd _createTopOnNativeAdInstance(ThirdAdParams thirdAdParams) {
  return TopOnNativeAd(thirdAdParams: thirdAdParams);
}
