import 'package:plugin_platform_interface/plugin_platform_interface.dart';

import 'top_on_ad_plugin_method_channel.dart';

abstract class TopOnAdPluginPlatform extends PlatformInterface {
  /// Constructs a TopOnAdPluginPlatform.
  TopOnAdPluginPlatform() : super(token: _token);

  static final Object _token = Object();

  static TopOnAdPluginPlatform _instance = MethodChannelTopOnAdPlugin();

  /// The default instance of [TopOnAdPluginPlatform] to use.
  ///
  /// Defaults to [MethodChannelTopOnAdPlugin].
  static TopOnAdPluginPlatform get instance => _instance;
  
  /// Platform-specific implementations should set this with their own
  /// platform-specific class that extends [TopOnAdPluginPlatform] when
  /// they register themselves.
  static set instance(TopOnAdPluginPlatform instance) {
    PlatformInterface.verifyToken(instance, _token);
    _instance = instance;
  }

   Future<T?> invokeNativeMethod<T>(String methodName) {
    throw UnimplementedError('invokeNativeMethod has not been implemented.');
  }
}
