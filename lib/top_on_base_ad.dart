import 'package:ad_common/third_ad.dart';
import 'package:ad_common/third_ad_instance.dart';
import 'package:ad_common/util/log.dart';
import 'package:anythink_sdk/at_index.dart';
import 'package:top_on_ad_plugin/topOn_ad_manager.dart';

/// FileName top_on_base_ad
/// @Author zhuqingfang
/// @Date 2022/11/18 16:05
/// @Description top on ad base class
class TopOnBaseAd extends ThirdAd {
  TopOnBaseAd({required ThirdAdParams thirdAdParams})
      : super(thirdAdParams: thirdAdParams);

  @override
  void load() {
    //初始化top on sdk
    init(thirdAdParams: thirdAdParams);

    //传入userid、channelId 等用户定制信息
    _setCustomDataDic(thirdAdParams);
  }

  @override
  void show() {}
}

//检查广告状态
void checkAdState(
    {required ThirdAdType thirdAdType,
    required String placementId,
    required Function(bool isLoaded) isLoaded}) async {
  Map adState = {};
  switch (thirdAdType) {
    case ThirdAdType.topOnRewardVideoAd:
      adState = await ATRewardedManager.checkRewardedVideoLoadStatus(
        placementID: placementId,
      );
      break;
    case ThirdAdType.topOnInsertAd:
      adState = await ATInterstitialManager.checkInterstitialLoadStatus(
          placementID: placementId);
      break;
    case ThirdAdType.topOnBannerAd:
      adState =
          await ATBannerManager.checkBannerLoadStatus(placementID: placementId);
      break;
    case ThirdAdType.topOnNativeAd:
      adState = await ATNativeManager.checkNativeAdLoadStatus(
          placementID: placementId);
      break;
    default:
  }
  if (adState.containsKey('isLoading')) {
    adState['isLoading'] is bool
        ? adState['isLoading'] == true
            ? isLoaded(true)
            : isLoaded(false)
        : adState['isLoading'] == "0"
            ? isLoaded(false)
            : isLoaded(true);
    return;
  }
  if (adState.containsKey('isReady')) {
    adState['isReady'] is bool
        ? adState['isReady'] == true
            ? isLoaded(true)
            : isLoaded(false)
        : adState['isReady'] == "NO"
            ? isLoaded(false)
            : isLoaded(true);
    return;
  }
  if (!adState.containsKey('isLoading') && !adState.containsKey('isReady')) {
    isLoaded(false);
  }
}

//是否可以展示广告
void isShowTopOnAd(
    {required ThirdAdType thirdAdType,
    required String placementId,
    required Function(bool isShow) isShowAd}) async{
  bool isShow = false;
  switch (thirdAdType) {
    case ThirdAdType.topOnRewardVideoAd:
      isShow = await ATRewardedManager.rewardedVideoReady(
        placementID: placementId,
      );
      break;
    case ThirdAdType.topOnInsertAd:
      isShow = await ATInterstitialManager.hasInterstitialAdReady(
          placementID: placementId);
      break;
    case ThirdAdType.topOnBannerAd:
      isShow = await ATBannerManager.bannerAdReady(placementID: placementId);
      break;
    case ThirdAdType.topOnNativeAd:
      isShow = await ATNativeManager.nativeAdReady(placementID: placementId);
      break;
    default:
  }
  isShowAd(isShow);
}

//userId 可能会变化，所以每次加载广告都会调用
_setCustomDataDic(ThirdAdParams thirdAdParams) {
  Log.i(
      'top on set userId:${thirdAdParams.userId} channel:${thirdAdParams.channel} segment:${thirdAdParams.segmentId}');
  ATInitManger.setCustomDataMap(
    customDataMap: {
      'user_id': thirdAdParams.userId ?? '0',
      'channel': thirdAdParams.channel ?? 'default',
      'sub_channel': thirdAdParams.subChannel ?? "default"
    },
  );
  if (thirdAdParams.segmentId != null) {
    ATInitManger.setPlacementCustomData(
        placementCustomDataMap: {'segment_id': thirdAdParams.segmentId!},
        placementIDStr: thirdAdParams.placementId);
  }
}
