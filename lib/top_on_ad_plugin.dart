
import 'top_on_ad_plugin_platform_interface.dart';

class TopOnAdPlugin {
  Future<T?> invokeNativeMethod<T>(String methodName) {
    return TopOnAdPluginPlatform.instance.invokeNativeMethod(methodName);
  }
}
