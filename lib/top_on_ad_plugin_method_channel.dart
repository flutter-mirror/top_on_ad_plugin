import 'package:flutter/foundation.dart';
import 'package:flutter/services.dart';
import 'package:top_on_ad_plugin/topOn_ad_manager.dart';

import 'top_on_ad_plugin_platform_interface.dart';
/// An implementation of [TopOnAdPluginPlatform] that uses method channels.
class MethodChannelTopOnAdPlugin extends TopOnAdPluginPlatform {
  /// The method channel used to interact with the native platform.
  @visibleForTesting
  final methodChannel = const MethodChannel('top_on_ad_plugin');


  @override
  Future<T?> invokeNativeMethod<T>(String methodName) async{
    methodChannel.setMethodCallHandler(platformCallHandler);
    final value = await methodChannel.invokeMethod<T>(methodName);
    return value;
  }

  Future<String?> getPlatformVersion() async {
    methodChannel.setMethodCallHandler(platformCallHandler);
    final version = await methodChannel.invokeMethod<String>('getPlatformVersion');
    return version;
  }
  Future<dynamic> platformCallHandler(MethodCall call) async {

  }
}
